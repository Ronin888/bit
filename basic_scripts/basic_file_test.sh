#!/bin/bash - 
#===============================================================================
#
#          FILE: is_basic_file_test.sh
# 
#         USAGE: ./basic_file_test.sh packagename
# 
#   DESCRIPTION: determines if a user specified package has been installed
#                Echo's "status" to standard output 
#                Returns exit code of 0 if it is.
# 
#       OPTIONS: ---
#  REQUIREMENTS: yum command must be available
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: 
#  ORGANIZATION: BCIT
#       CREATED:
#      REVISION: 1.0
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable Declarations
# See man declare (part of bash builtins)
# Deprecated Alternatives to the declare command: typeset, local
# Switches 
#   -a     indexed array 
#   -A     associative array 
#   -f     function 
#   -i     integer, arithmetic evaluation is performed when the variable is assigned a value.
#   -l     all upper-case characters are converted to lower-case.
#   -r     readonly, cannot be assigned values by assignment statements 
#   -u     all lower-case characters are converted to upper-case
#   -x     Mark names for export to subsequent commands via the environment.

declare -r SCRIPTVERSION="1.0"
declare file_name=$1 
declare -i package_installed=1



#Store the command exit status
# See Bash Reference 3.4 Shell Parameters (3.4.2 Special Parameters) Specifically
package_installed=$?


#echo "The output from the yum list installed command is: $yum_output"

if [ -e $file_name ] ; then
  echo "$file_name exists"
  exit 0 
else
  echo "$file_name can't be found"
  exit 1
fi





