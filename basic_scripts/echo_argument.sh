#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_argument.sh
# 
#         USAGE: ./echo_main.sh 
# 
#   DESCRIPTION: This demonstates the ability of one script to include others
# 
#       OPTIONS: ---
#  REQUIREMENTS: 
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: 
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
#declare script_name=$0
echo -e "\nBeginning $1"


echo -e "\nEnding $1"

