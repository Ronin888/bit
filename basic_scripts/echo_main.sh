#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_main.sh
# 
#         USAGE: ./source_master.sh 
# 
#   DESCRIPTION: This demonstates the ability of one script to include others
# 
#       OPTIONS: ---
#  REQUIREMENTS: source_sub1.sh and echo_argument.sh must live in the same
#                directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: 
#  ORGANIZATION: BCIT
#       CREATED: 
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare script_name=$0
echo -e "\nBeginning $script_name"

	 ./echo_argument.sh $0
#. ./echo_argument.sh

echo -e "\nEnding $script_name\n"

exit 0


