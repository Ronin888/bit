#!/bin/bash - 


	func_test () {

	set -o nounset           # Treat unset variables as an error


	declare -r SCRIPTVERSION="1.0"
	declare file_name=$1 
	declare -i package_installed=1

	package_installed=$?

	if [ -e $file_name ] ; then
  	  echo "$1 exists"
  	  exit 0 
	else
  	  echo "$1 can't be found"
  	  exit 0
	fi

	}



