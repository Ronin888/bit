#!/bin/bash -


	set -o nounset           # Treat unset variables as an error


	declare -r SCRIPTVERSION="1.0"


output() {


	for((i=1; i <=$#; i++)); do

		declare file_name=${!i}
		declare -i package_installed=1

		package_installed=$?


		if [ -e $file_name ] ; then
  		  echo "$file_name exists"

		else
	  	  echo "$file_name can't be found"
  		  exit 0
		fi

	done

}

output $@
