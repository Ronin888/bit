#!/bin/bash


if [ $# -lt 2 ]; then
        echo "Insufficient number of arguments input"
        exit 1
fi


case "$2" in

1)  echo "Sending SIGHUP signal to procees $1"
    kill -SIGHUP $1
    ;;
2)  echo  "Sending SIGINT signal to procees $1"
    kill -SIGINT $1
    ;;
15)  echo  "Sending SIGQUIT signal to procees $1"
    kill -SIGQUIT $1
    ;;
30) echo  "Sending SIGKILL signal to procees $1"
   kill -SIGKILL $1
   ;;
*) echo "Signal number $2 is not processed as invalid signal ID supplied."
   ;;

 esac
