#!/bin/bash - 

set -o nounset                              # Treat unset variables as an error

#Variable Declarations
# See man declare (part of bash builtins)
# Deprecated Alternatives to the declare command: typeset, local
# Switches 
#   -a     indexed array 
#   -A     associative array 
#   -f     function 
#   -i     integer, arithmetic evaluation is performed when the variable is assigned a value.
#   -l     all upper-case characters are converted to lower-case.
#   -r     readonly, cannot be assigned values by assignment statements 
#   -u     all lower-case characters are converted to upper-case
#   -x     Mark names for export to subsequent commands via the environment.

declare -r SCRIPTVERSION="1.0"
declare whereis_output  
declare -i package_installed=1


whereis_output=$( whereis $1 2> /dev/null )


locations=$( (echo $whereis_output | cut -d ":" -f 2-) )


if [[ "$locations" == "" ]]; then
  echo "$whereis_output is not installed"
  exit 0
else
  echo "$whereis_output is located: "
  exit 0
fi

