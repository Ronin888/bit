#!/bin/bash - 
#===============================================================================
#
#          FILE: exec_demo1.sh
# 
#         USAGE: ./exec_demo1.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/03/2015 20:17
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# Following sets the variable to the current script file name removing the 
#   leading path, specifically
# ${0} captures scriptname using parameter expansion
# ## invokes prefix removal 
# */ is the pattern to remove i.e all characters up to the last /
#   i.e. everything but the relative path name
declare -rx SCRIPT=${0##*/} # the variable is exported (-x) and readonly (-r) 


#declare a readonly variable for the script to be executed
declare -r exec2=”./exec_demo_script2.sh”

#Verify the that script2 is executable (-x) if not (!) 
if [[ ! -x exec2 ]] ; then
  printf "%s" “$SCRIPT:$LINENO: the script $exec2 is not executable” >&2
  exit 192
fi

printf “$SCRIPT: transferring control to $exec2, never to return...\n”
exec “$exec2”

# If the following line has been reached the exec statement didn't call the other
# script
printf “$SCRIPT:$LINENO: exec failed!\n” >&2

exit 1 #Make sure the caller knows the exec failed
