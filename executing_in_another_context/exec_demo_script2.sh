#!/bin/bash - 
#===============================================================================
#
#          FILE: exec_demo_script2.sh
# 
#         USAGE: ./exec_demo_script2.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/03/2015 20:59
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Get the name of the current script stripping off the leading path see 
# exec_demo1.sh for details
declare -rx SCRIPT=${0##*/}

printf "%s\n" "$SCRIPT: now running exec2.sh"

exit 0

