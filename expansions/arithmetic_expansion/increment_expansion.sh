#!/bin/bash - 
#===============================================================================
#
#          FILE: increment_expansion.sh
# 
#         USAGE: ./increment_expansion.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/26/2015 17:21
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i answer
declare -i number1=10

echo -e "\nPost Increment"
echo -e "Starting number1: $number1"
answer=$(( number1++ ))
echo -e "Answer: $answer"
echo -e "Number: $number1"

echo -e "\nPost Decrement"
echo -e "Starting number1: $number1"
answer=$(( number1-- ))
echo -e "Answer: $answer"
echo -e "Number: $number1"

echo -e "\nPre Increment"
echo -e "Starting number1: $number1"
answer=$(( ++number1 ))
echo -e "Answer: $answer"
echo -e "Number: $number1"

echo -e "\nPre Decrement"
echo -e "Starting number1: $number1"
answer=$(( --number1 ))
echo -e "Answer: $answer"
echo -e "Number: $number1"

echo -e "\nPre Decrement"
echo -e "Starting number1: $number1"
answer=$(( --number1 ))
echo -e "Answer: $answer"
echo -e "Number: $number1"


