#!/bin/bash - 
#===============================================================================
#
#          FILE: logical_negation.sh
# 
#         USAGE: ./logical_negation.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/26/2015 17:25
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i answer
declare -i number1=10
declare -i negative_number=-10
declare -ir ONE=1
declare -ir ZERO=0

echo -e "\nLogical negation"
echo -e "Starting number1: $number1"
answer=$(( !number1 ))
echo -e "Answer: $answer"
echo -e "number1: $number1"

echo -e "\nLogical negation"
echo -e "Starting negative_number: $negative_number"
answer=$(( !negative_number))
echo -e "Answer: $answer"
echo -e "Negative Number: $negative_number"

echo -e "\nLogical negation"
echo -e "Starting ONE: $ONE"
answer=$(( !ONE))
echo -e "Answer: $answer"
echo -e "ONE: $ONE"

echo -e "\nLogical negation"
echo -e "Starting ZERO: $ZERO"
answer=$(( !ZERO ))
echo -e "Answer: $answer"
echo -e "Number: $ZERO"


