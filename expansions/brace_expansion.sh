#!/bin/bash - 
#===============================================================================
#
#          FILE: brace_expansion.sh
# 
#         USAGE: ./brace_expansion.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: See: BASH Reference Manual 3.5.1 Brace Expansion for Details
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/25/2015 10:40
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Brace Expansion: Sequence Form
# Follows the form: 
#   {start..end..inc} 
# Where:
#   {}. are literal characters and must be typed as above
#   start is an integer or single character representing the start of the sequence
#   end is an integer or single character representing the end of the sequence
#   incr is an optional integer representing the increment of the sequence
echo -e "\nSequence Brace Expansion: No increment"
echo -e prefix_{2..20}.ext"\n"

echo -e "\nSequence Brace Expansion: increment"
echo -e prefix_{02..20..2}.ext"\n"

echo -e "\nSequence Brace Expansion: character"
echo -e prefix_{A..Z..2}.ext"\n"


#Brace Expansion: Comma Seperated String Form
# Follows the form: 
#   {string1,string2,string3} 
# Where:
#   {}, are literal characters and must be typed as above
#   string1, string2, string3 are strings that will be expanded in sequence
echo -e "\nComma Seperated String Brace Expansion"
echo -e {notes,config,testing}.txt"\n" 

echo -e "\nComma Seperated String Brace Expansion: Multiple Expansion"
echo -e {notes,config,testing}.{txt,md,odf}"\n" 
