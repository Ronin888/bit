#!/bin/bash - 
#===============================================================================
#
#          FILE: pattern_matching.sh
# 
#         USAGE: ./pattern_matching.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: See http://www.tldp.org/LDP/abs/html/globbingref.html
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/28/2015 22:27
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
# Special Globbing Characters
#   * Wildcard one or more characters
#   ? Single Character Wildcard
#   [] lists of characters

ls /[rs]* #list all directories in root that start with r or s
ls /etc/*.???? #list all directores in /etc with four character extensions
