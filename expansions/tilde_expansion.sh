#!/bin/bash - 
#===============================================================================
#
#          FILE: tilde_expansion.sh
# 
#         USAGE: ./tilde_expansion.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: See BASH Reference Manual 3.5.2 Tilde Expansion For Details
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/25/2015 11:10
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#HOME Varialbe expansion
echo ~

#User Name home directory expansion
echo ~root

#PWD expansion
echo ~+
echo ~_

#pushd, popd, dirs related expansions
pushd /
pushd /home
popd 
echo ~+1
echo ~1
echo ~-1


