#!/bin/bash - 
#===============================================================================
#
#          FILE: simple_functions.sh
# 
#         USAGE: ./simple_functions.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/30/2015 12:16
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Function Declarions
function func1 {
  echo "Doing stuff in func1, here is arg1: $1"
  return 0
}

func2() {
  echo "Doing stuff in func2, here is arg2: $2"
  echo "Fisrt Arg: $1"
  return 0
}

echo "Before function invocation"

#Here the functions are invoked i.e used
for ((i=1; i<=5; i++)); do
  func1 $i
done
func2 stuff "This will be the second argument for func2"













