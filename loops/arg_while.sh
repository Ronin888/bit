#!/bin/bash - 
#===============================================================================
#
#          FILE: arg_while.sh
# 
#         USAGE: ./arg_while.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Demonstrates looping over arguments using while loop
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/29/2015 15:54
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare -i arg=1;                           # Set arg to be equal to first parameter

#Loop while arg is less than or equal to the total number of  parameters passed to 
#the script ( $#)
# (( )) integer tests don't require $ expansion
i=1
while (( arg <= 5)) ; do
#echo $arg

  #Increment the argument counter to point to the next argument 
  # i.e. move from $1 to $2
  arg=$(( ++arg ))
  echo $((++i))
  echo "$i"
  i=1;
done

