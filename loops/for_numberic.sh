#!/bin/bash - 
#===============================================================================
#
#          FILE: simple_for_numeric.sh
# 
#         USAGE: ./simple_for_numeric.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/03/2015 18:58
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare -i num_times
read -p "Enter the number of times the loop should run:" num_times 

for (( counter=1; counter<=num_times; counter+=1 )); do
  printf "\n%s" "Interation: $counter"
done
