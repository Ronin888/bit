#!/bin/bash - 
#===============================================================================
#
#          FILE: simple_while_numeric.sh
# 
#         USAGE: ./simple_while_numeric.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/03/2015 19:09
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare -i num_times
declare -i counter=1
read -p "Enter the number of times the loop should run:" num_times 


while ((counter <= num_times)) ; do
  printf "\n%s" "Interation: $counter"
  ((counter++))
done


