#!/bin/bash - 
#===============================================================================
#
#          FILE: install_vbox_epel.sh
# 
#         USAGE: ./install_vbox_epel.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Requires VB Guest Additions are in Virtual CD
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/05/2015 08:19
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# :TODO:05/07/2015 18:00:tl: add check to verify if extensions are prexisting
# check for kernel module: modinfo vboxguest

#Install Virtualbox Guest Additions
yum -y install kernel-devel kernel-headers dkms gcc gcc-c++ bzip bzip2 
mkdir vboxcd
mount /dev/cdrom ./vboxcd
cd ./vboxcd
sh ./VBoxLinuxAdditions.run

#Install epel
curl -O http://mirror.its.dal.ca/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
yum install ./epel-release-7-5.noarch.rpm
yum ./epel-release-7.5.noarch.rpm
