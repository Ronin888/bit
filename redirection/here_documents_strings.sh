#!/bin/bash - 
#===============================================================================
#
#          FILE: here_documents_strings.sh
# 
#         USAGE: ./here_documents_strings.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/09/2015 09:00
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Here Document
cat << TEXTDELIMITER
All of the text between the arbitrary delimeters 
forms the content of the "here document"
TEXTDELIMITER

#Simple Here String
declare here_string_content="This content will form the body of the here string"
cat <<< $here_string_content

#Capturing output into a here string
declare your_name=$(whoami)
cat <<< $your_name
