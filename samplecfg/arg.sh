#!/bin/bash - 
#===============================================================================
#
#          FILE: arg.sh
# 
#         USAGE: ./arg.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 04/29/2016 10:39
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an errora

if [[ $# -eq 0 ]]; then
 echo 'please specify command'
 exit -1
fi

which $1

source ./echo.sh

echo "execution back in $0"
