#!/bin/bash - 
#===============================================================================
#
#          FILE: part4.sh
# 
#         USAGE: ./part4.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 05/01/2016 10:38
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# 1. grab the expression
myvar=$( (echo "range 192.168.1.10 192.168.1.100;" | grep -Eo 'range[[:space:]]*[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.') )
echo "$myvar"

# 2. replace the expression
echo "range 192.168.1.10 192.168.1.100;" | sed s/'range[[:space:]]*[0-9]*.[0-9]*.[0-9]*.'/'MYEXPR'/


# 3. insert the new range
echo 'MYEXPR10' | sed s/'MYEXPR[0-9]{1,3}'/'MYEXPR13'/


# 4. put the saved expression back
echo 'MYEXPR13' | sed s/'MYEXPR'/"$myvar"/
