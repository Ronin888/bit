#!/bin/bash - 
#===============================================================================
#
#          FILE: routersetup.sh
# 
#         USAGE: ./routersetup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 04/24/2016 16:48
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# set the server to static :: THIS IS JUST AN EG
cp ifcfg-eno16777736 old
sed s/'dhcp'/'static'/ old > ifcfg-eno16777736
rm old

