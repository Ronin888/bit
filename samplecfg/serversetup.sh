#!/bin/bash - 
#===============================================================================
#
#          FILE: serversetup.sh
# 
#         USAGE: ./serversetup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: -r means configure server
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 04/24/2016 16:39
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

task=$1
router='r'
serverHostName=$2

echo "$router"

source ./basicSetup.sh serverHostName

if [[ "$task" == "$router" ]] ; then
  source ./routersetup.sh
fi

