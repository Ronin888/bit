#!/bin/bash - 
#===============================================================================
#
#          FILE: sed_a.sh
# 
#         USAGE: ./sed_a.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/08/2015 21:28
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare new_text="This is new stuff"

#add text to field
sed '/[238]/a\ this goes after lines with a 2,3,8' template.txt

#add text
sed '/#To_be_added_start/a\this goes after #To_be_added_start' template.txt

#add text from a variable
sed "/#To_be_added_start/a\\$new_text" template.txt



