#!/bin/bash - 
#===============================================================================
#
#          FILE: sed_add_from_template.sh
# 
#         USAGE: ./sed_add_from_template.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/08/2015 21:57
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare temp_text
#use two search and replace expressions to change the parameters in the instertion template, then store the values in temp_text
temp_text=$(sed -e 's/<param1>/"value for parameter 1"/' -e 's/<param2>/"parameter 2 data"/' insertion_template.txt)
echo $temp_text

#Escape all of the quotations in the temp text and save the result back to temp_text
#also note the <<< which is a here string with expansion
temp_text=$(sed 's|"|\"|g' <<< $temp_text)
echo $temp_text

#Append the temp text to the destination.txt after the lines that match Section2
#note it is necessary to escape the \ character since it preceds the $ character
sed "/Section2/a\\$temp_text" destination.txt

