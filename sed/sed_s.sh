#!/bin/bash - 
#===============================================================================
#
#          FILE: sed_s.sh
# 
#         USAGE: sed_s.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/06/2015 09:30
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


##Search and Replace i.e. s command

# search for the first occurance of variable and replace it with var on everyline 
sed 's/variable/var/' template.txt

# search for all of the occurances of variable and replace them with var on everyline "
sed 's/variable/var/g' template.txt

# search for  all of the occurances of variable and replace them with var on lines 4 to 7
sed '4,7 s/variable/var/g' template.txt

# search for  all of the occurances of variable and replace them with var on lines 
# with an even numbered variable"
sed '/variable[2468]/ s/variable/var/g' template.txt

# search for  all of the occurances of a an angle bracket followed by an underscore
# then a digit and then the closing angle bracket
# Replace them with the value of the digit"
# Do this for all even lines"
sed '0~2 s/<.*_\([[:digit:]]\)>/\1/g' template.txt

# search for  all of the occurances of a string of alphabetic characters 2 to
# 12 long followed by an underscore and a digit.\n\
# Replace them with the value of the digit"
# Do this for all lines with an even numbered variable"
sed '/variable[2468]/ s/[[:alpha:]]\{2,12\}_\([[:digit:]]\)\{1\}/\1/g' template.txt
