#!/bin/bash - 
#===============================================================================
#
#          FILE: source_master.sh
# 
#         USAGE: ./source_master.sh 
# 
#   DESCRIPTION: This demonstates the ability of one script to include others
# 
#       OPTIONS: ---
#  REQUIREMENTS: source_sub1.sh and source_sub2.sh must live in the same
#                directory.
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 13:31
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare script_name=$0
echo -e "\nBeginning $script_name"

source ./source_sub1.sh
. ./source_sub2.sh

echo -e "\nEnding $script_name\n"
