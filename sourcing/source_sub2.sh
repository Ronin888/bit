#!/bin/bash - 
#===============================================================================
#
#          FILE: source_sub2.sh
# 
#         USAGE: ./source_sub1.sh 
# 
#   DESCRIPTION: This is part of the sourcing demonstration
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: This script is intended to be invoked from source_master.sh
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/24/2015 13:32
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
echo -e "\n\tBeginning sub2"
echo -e "\tEnding sub2"
