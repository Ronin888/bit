#!/bin/bash - 
#===============================================================================
#
#          FILE: basic_conditional_tests.sh
# 
#         USAGE: ./basic_conditional_tests.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Demonstrates form of conditional test
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/29/2015 19:32
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare home_dir="/home"

# test command form
if test -e $home_dir  ; then
  echo "home directory exists" 
fi

# single [ form
if [ -e $home_dir ] ; then
  echo "home directory exists" 
fi

# Recommended [[ form
if [[ -e $home_dir ]] ; then
  echo "home directory exists" 
fi


