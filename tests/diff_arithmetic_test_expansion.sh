#!/bin/bash - 
#===============================================================================
#
#          FILE: diff_arithmetic_test_expansion.sh
# 
#         USAGE: ./diff_arithmetic_test_expansion.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/30/2015 10:51
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
#echo ((5 + 3))                              #comment out this line so example runs
echo $((5 + 3))

(( 5 + 3)) 
echo $?

(( 5 - 5 ))
echo $?
