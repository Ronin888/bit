#!/bin/bash - 
#===============================================================================
#
#          FILE: conditional_tests.sh
# 
#         USAGE: ./conditional_tests.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/29/2015 19:32
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
declare file_name_w_space="some file name with spaces.txt"

touch "$file_name_w_space"

# test and [ don't automatically escape (i.e. quote) expanded parameters
# hence the following will cause errors
if test -a $file_name_w_space ; then
  echo "the file with a name exists" 
fi

if [ -a $file_name_w_space ] ; then
  echo "the file with a name exists" 
fi

#The recommended [[ expression automatically expands parameters

if [[ -a $file_name_w_space ]] ; then 
  echo "the file with a name exists" 
fi

#It is possible to use the first methods but note the extra escaping
if test -a "$file_name_w_space" ; then
  echo "the file with a name exists" 
fi

if [ -a "$file_name_w_space" ] ; then
  echo "the file with a name exists" 
fi


rm "$file_name_w_space"

