#!/bin/bash - 
#===============================================================================
#
#          FILE: called_script.sh
# 
#         USAGE: ./called_script.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/27/2015 11:22
#      REVISION:  ---
#===============================================================================

echo -e "\n**** Called Script Output ****"
echo -e "ms_declare: \t\t$ms_declare"
echo -e "ms_not_declared: \t$ms_not_declared"
echo -e "ms_declared_export: \t$ms_declared_export"
echo -e "MS_EXPORT: \t\t$MS_EXPORT"
echo -e "func_local: \t\t$func_local"
echo -e "func_not_declared: \t$func_not_declared"
echo -e "func_declared_export: \t$func_declared_export"
echo -e "FUNC_EXPORT: \t\t$FUNC_EXPORT"
echo -e "**** End Called Script Output ****\n"

