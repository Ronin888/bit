#!/bin/bash - 
#===============================================================================
#
#          FILE: vim_bash_setup.sh
# 
#         USAGE: ./vim_bash_setup.sh 
# 
#   DESCRIPTION: Installs vim plugins for bash editing and updates vim
#                configuration for editing of scripts.
#                 
#                Plugins: 
#                  vundle - for package management
#                  bash-support - for bash scriting enhancements
#               
#                File Modifications
#                  ~/.vimrc 
#                  ~/.vim/after/ftplugin/sh.vim
# 
#       OPTIONS: ---
#  REQUIREMENTS: git and vim packages
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane 
#  ORGANIZATION: 
#       CREATED: 12/04/15 16:37
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
vundle_dir=~/.vim/bundle/Vundle.vim
vundle_git_repo="https://github.com/gmarik/Vundle.vim.git"

#Install vundle not already installed
if  [[ ! -d $vundle_dir ]] ; then
  git clone  $vundle_git_repo $vundle_dir 
fi

#Install vim support
#This is done by adding plugin configuration to ~/.vimrc

#Backup existing file
if [[ -e ~/.vimrc ]]; then
 mv ~/.vimrc ~/.vimrc_old
 echo 'Manually merge your old ~/.vimrc file (now ~/.vimrc_old) with' 
 echo 'the newly updated ~/.vimrc file'
fi

#Write new file
cat <<- 'VIMRCFile' > ~/.vimrc 
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'vim-scripts/bash-support.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
VIMRCFile

#Execute plugin installation via command line
vim -c ":PluginInstall" -c ":PluginUpdate" -c ":q" -c ":q"

#Configure .sh and .bash specific settings
#This done by creating ~/.vim/after/ftplugin/sh.vim 

#Backup existing sh.vim configuration file
if [[  -e  ~/.vim/after/ftplugin/sh.vim ]] ; then
  mv ~/.vim/after/ftplugin/sh.vim ~/.vim/after/ftplugin/sh.vim.old
  echo 'Manually merge your old ~/.vim/after/ftplugin/sh.vim file '   
  echo '(now ~/.vim/after/ftplugin/sh.vim.old ) with the newly updated ~/.vimrc file'
fi

if  [[ ! -d  ~/.vim/after/ftplugin/ ]] ; then
  mkdir -p ~/.vim/after/ftplugin
fi

cat <<- 'SHVIMFile' > ~/.vim/after/ftplugin/sh.vim
"Configuration for Bash Editing
set number
set autoindent
set copyindent
set tabstop=2
set shiftwidth=2
set expandtab
set showmatch
set title
set smarttab
set smartcase
set nowrap
SHVIMFile
